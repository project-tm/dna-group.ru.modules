<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Project\Tools\Modules;

if(!Loader::includeModule('jerff.core')) {
    return;
}

Loc::loadMessages(__FILE__);

class dna_demomarket extends CModule
{

    public $MODULE_ID = 'dna.demomarket';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, 'DNA_DEMOMARKET');
        $this->MODULE_NAME = Loc::getMessage('DNA_DEMOMARKET_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DNA_DEMOMARKET_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('DNA_DEMOMARKET_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DNA_DEMOMARKET_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    /*
     * InstallEvent
     */

    public function InstallEvent()
    {
        $this->registerEventHandler('main', 'OnPageStart', '\Dna\Demomarket\Event\Page', 'OnPageStart');
    }

    public function UnInstallEvent()
    {
        $this->unRegisterEventHandler('main', 'OnPageStart', '\Dna\Demomarket\Event\Page', 'OnPageStart');
    }

    public function InstallAgent()
    {
        /* User */
        $this->InstallAgentTime('Dna\Demomarket\Agent\Oper\Demomarket::import();', 60);
        $this->InstallAgentTime('Dna\Demomarket\Agent\Oper\Dnahobby::import();', 60);

        /* Catalog */
        $this->AddAgent(
            'Dna\Demomarket\Agent\Catalog\Edding::demomarket();',
            'N',
            60 * 60 * 4,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString()
        );
        $this->AddAgent(
            'Dna\Demomarket\Agent\Catalog\Edding::dnahobby();',
            'N',
            60 * 60 * 4,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString()
        );

        /* Price */
        $this->InstallAgentTime('Dna\Demomarket\Agent\Price\Import::update();', 30);
        $this->InstallAgentTime('Dna\Demomarket\Agent\Price\Export::demomarket();', 5 * 60);
        $this->InstallAgentTime('Dna\Demomarket\Agent\Price\Export::dnahobby();', 5 * 60);
        $this->AddAgent(
            'Dna\Demomarket\Agent\Price\Export::clear();',
            'Y',
            60 * 60 * 24,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->add('1 day')->toString()
        );
    }

    public function InstallAgentTime($name, $interval)
    {
        $this->AddAgent(
            $name,
            'N',
            $interval,
            (new DateTime())->toString(),
            'Y',
            (new DateTime())->toString()
        );
    }

    public function UnInstallAgent()
    {
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Oper\Demomarket::import();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Oper\Dnahobby::import();', $this->MODULE_ID);

        CAgent::RemoveAgent('Dna\Demomarket\Agent\Catalog\Edding::demomarket();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Catalog\Edding::dnahobby();', $this->MODULE_ID);

        CAgent::RemoveAgent('Dna\Demomarket\Agent\Price\Import::update();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Price\Export::demomarket();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Price\Export::dnahobby();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Demomarket\Agent\Price\Export::clear();', $this->MODULE_ID);
        return true;
    }

}
