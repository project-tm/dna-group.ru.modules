<?

namespace Dna\Demomarket\Agent\Oper;

use Bitrix\Main\Loader;
use CCatalogProduct;
use CCatalogStoreProduct;
use CIBlockElement;
use CPrice;
use Dna\Demomarket\Config;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\Settings;

class Dnahobby
{

    const UPLOAD_PATH_OPER = Config::UPLOAD_PATH . "oper.csv";

    /**
     * @return string
     */
    public static function import()
    {
        Cron::agent(function () {
            $hash = sha1_file(self::UPLOAD_PATH_OPER);
            if (Settings::get(__CLASS__) == $hash) {
                return;
            }

            Loader::IncludeModule("iblock");
            Loader::IncludeModule("catalog");

            set_time_limit(0);

            $sku_iblock_id = Iblock::getByCodeOnly(Config::IBLOCK_ELEMENT_DNAHOBBY);

            $row = 1;
            $row_affected = 0;
            if (($handle = fopen(self::UPLOAD_PATH_OPER, "r")) !== false) {
                $el = new CIBlockElement;
                while (($data = fgetcsv($handle, 0, ";")) !== false) {
                    if ($row > 1) {

                        foreach ($data as $key => $val) {
                            $data[$key] = mb_convert_encoding($val, 'utf-8', 'Windows-1251');
                            $data[$key] = trim($data[$key]);
                            $data[$key] = str_replace('"', '', $data[$key]);
                        }

                        $arFields = [
                            "NAME"      => $data[0],
                            "IBLOCK_ID" => $sku_iblock_id,
                        ];
                        $rsElem = CIBlockElement::GetList([], ['IBLOCK_ID' => $sku_iblock_id, '=NAME' => $data[0]],
                            false, false, ['ID']);
                        if ($arElem = $rsElem->Fetch()) {
                            $PRODUCT_ID = $arElem["ID"];
                            $el->Update($PRODUCT_ID, $arFields);


                            if ($data[5] != '') {
                                $arFields['SPECPRICE'] = $data[5];
                            }
                            if ($data[6] != '') {
                                $arFields['TMPPRICE'] = $data[6];
                            }
                            if ($data[3] != '') {
                                $arFields['WODISCOUNTPRICE'] = $data[2];
                            } else {
                                $arFields['WODISCOUNTPRICE'] = '';
                            }
                            if ($arFields) {
                                CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, $arFields);
                            }

                            //Устанавливаем вес
                            $db_res = CCatalogProduct::GetList([], ["ID" => $PRODUCT_ID]);
                            $catArr = ["ID" => $PRODUCT_ID, "QUANTITY" => $data[1]];
                            if (($ar_res = $db_res->Fetch())) {
                                CCatalogProduct::Update($PRODUCT_ID, $catArr);
                            } else {
                                CCatalogProduct::Add($catArr);
                            }

                            //Устанавливаем цену
                            $db_res = CPrice::GetList([], ["PRODUCT_ID" => $PRODUCT_ID, "CATALOG_GROUP_ID" => 1]);
                            $priceArr = [
                                "PRODUCT_ID"       => $PRODUCT_ID,
                                "CATALOG_GROUP_ID" => 1,
                                "PRICE"            => $data[3] != '' ? $data[3] : $data[2],
                                "CURRENCY"         => "RUB",
                            ];
                            if ($ar_res = $db_res->Fetch()) {
                                CPrice::Update($ar_res["ID"], $priceArr);
                            } else {
                                CPrice::Add($priceArr);
                            }

                            //Устанавливаем остаток на складе в МСК
                            $arAmoutn = [
                                1 => 7,
                                2 => 8,
                                3 => 9,
                            ];
                            $rsStore = CCatalogStoreProduct::GetList([], ['PRODUCT_ID' => $PRODUCT_ID, 'STORE_ID' => 1],
                                false, false, ['ID']);
                            while ($arStore = $rsStore->Fetch()) {
                                if (isset($arAmoutn[$arStore['STORE_ID']])) {
                                    CCatalogStoreProduct::Update($arStore['ID'],
                                        ["AMOUNT" => $data[$arAmoutn[$arStore['STORE_ID']]]]);
                                    unset($arAmoutn[$arStore['STORE_ID']]);
                                }
                            }
                            foreach ($arAmoutn[$arStore['STORE_ID']] as $key => $value) {
                                CCatalogStoreProduct::Add([
                                    "PRODUCT_ID" => $PRODUCT_ID,
                                    "STORE_ID"   => $key,
                                    "AMOUNT"     => $data[$value],
                                ]);
                            }
                        }
                        $row_affected = $row_affected + 1;
                    }
                    $row++;
                }

                fclose($handle);
                Settings::set(__CLASS__, $hash);
            }
        });
        return 'Dna\Demomarket\Agent\Oper\Dnahobby::import();';
    }

}
