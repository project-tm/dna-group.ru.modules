<?

namespace Dna\Demomarket\Agent\Oper;

use Bitrix\Main\Loader;
use CCatalogProduct;
use CCatalogStoreProduct;
use CIBlockElement;
use CPrice;
use Dna\Demomarket\Config;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\Settings;

class Demomarket
{

    const UPLOAD_PATH_OPER = Config::UPLOAD_PATH . "oper.csv";

    /**
     * @return string
     */
    public static function import()
    {
        Cron::agent(function () {
            $hash = sha1_file(self::UPLOAD_PATH_OPER);
            if (Settings::get(__CLASS__) == $hash) {
                return;
            }

            Loader::IncludeModule("iblock");
            Loader::IncludeModule("catalog");

            $sku_iblock_id = Iblock::getByCodeOnly(Config::IBLOCK_DEMOMARKET);

            $row = 1;
            if (($handle = fopen(self::UPLOAD_PATH_OPER, "r")) !== FALSE) {
                $el = new CIBlockElement;
                while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                    if($row>1){

                        foreach($data as $key=>$val)
                        {
                            $data[$key] = mb_convert_encoding($val, 'utf-8', 'Windows-1251');
                            $data[$key] = trim($data[$key]);
                            $data[$key] = str_replace('"','',$data[$key]);
                        }

                        $arFields = array(
                            "XML_ID" => $data[0],
                            "IBLOCK_ID" => $sku_iblock_id,
                        );

                        $rsElem = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $sku_iblock_id, 'XML_ID'=>$data[0]), false, false, array('ID', 'NAME'));
                        if($arElem = $rsElem->Fetch())
                        {
                            $arFields["NAME"] =  $arElem["NAME"];
                            $PRODUCT_ID = $arElem["ID"];
                            $el->Update($PRODUCT_ID, $arFields);


                            if ($data[5] != '') {
                                $arFields['SPECPRICE'] = $data[5];
                            }
                            if ($data[6] != '') {
                                $arFields['TMPPRICE'] = $data[6];
                            }
                            if ($data[3] != '') {
                                $arFields['WODISCOUNTPRICE'] = $data[2];
                            } else {
                                $arFields['WODISCOUNTPRICE'] = '';
                            }
                            if ($arFields) {
                                CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, $arFields);
                            }

                            //Устанавливаем вес
                            $db_res = CCatalogProduct::GetList(array(), array("ID" => $PRODUCT_ID));
                            $catArr = array("ID"=>$PRODUCT_ID, "QUANTITY"=>$data[1]);
                            if(($ar_res = $db_res->Fetch()))
                            {
                                CCatalogProduct::Update($PRODUCT_ID, $catArr);
                            }
                            else
                            {
                                CCatalogProduct::Add($catArr);
                            }

                            //Устанавливаем цену
                            $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $PRODUCT_ID, "CATALOG_GROUP_ID" => 1));
                            $priceArr = Array(
                                "PRODUCT_ID" => $PRODUCT_ID,
                                "CATALOG_GROUP_ID" => 1,
                                "PRICE" => $data[3]!=''?$data[3]:$data[2],
                                "CURRENCY" => "RUB"
                            );
                            if ($ar_res = $db_res->Fetch())
                            {
                                CPrice::Update($ar_res["ID"], $priceArr);
                            }
                            else
                            {
                                CPrice::Add($priceArr);
                            }

                            $arAmoutn = [
                                1 => 7,
                                2 => 8,
                                3 => 9,
                            ];
                            $rsStore = CCatalogStoreProduct::GetList([], ['PRODUCT_ID' => $PRODUCT_ID, 'STORE_ID' => 1],
                                false, false, ['ID']);
                            while ($arStore = $rsStore->Fetch()) {
                                if (isset($arAmoutn[$arStore['STORE_ID']])) {
                                    CCatalogStoreProduct::Update($arStore['ID'],
                                        ["AMOUNT" => $data[$arAmoutn[$arStore['STORE_ID']]]]);
                                    unset($arAmoutn[$arStore['STORE_ID']]);
                                }
                            }
                            foreach ($arAmoutn[$arStore['STORE_ID']] as $key => $value) {
                                CCatalogStoreProduct::Add([
                                    "PRODUCT_ID" => $PRODUCT_ID,
                                    "STORE_ID"   => $key,
                                    "AMOUNT"     => $data[$value],
                                ]);
                            }
                        }
                    }
                    $row++;
                }

                fclose($handle);
                Settings::set(__CLASS__, $hash);
            }

        });
        return 'Dna\Demomarket\Agent\Oper\Demomarket::import();';
    }

}
