<?

namespace Dna\Demomarket\Agent\Price;

use Bitrix\Main\Loader;
use Dna\Demomarket\Config;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\Settings;

class Import
{
    const UPLOAD_PATH_CATALOG = Config::UPLOAD_PATH . "catalog.csv";
    const LIMIT_STEP = 30;
    /**
     *
     */
    public static function update()
    {
        Cron::agent(function () {
            return;

            $hash = sha1_file(self::UPLOAD_PATH_CATALOG);
            if (Settings::get(__CLASS__) == $hash) {
                return;
            }

            Loader::IncludeModule("iblock");
            Loader::IncludeModule("catalog");

            set_time_limit(0);

            $step = Settings::get(__CLASS__ . 'step');
            if (empty($step) or $step < 1) {
                $step = 1;
            } else {
                $step = (int)$step;
            }

            pre($step);

            Settings::set(__CLASS__ . 'step', ++$step);
            if (0) {
                Settings::set(__CLASS__, $hash);
            }
        });
        return 'Dna\Demomarket\Agent\Price\Import::update();';
    }

    /**
     *
     */
    public static function clear()
    {
        DeleteDirFilesEx(Config::PRICE_XLX_PATH);
        return 'Dna\Demomarket\Agent\Price\Import::clear();';
    }

}
