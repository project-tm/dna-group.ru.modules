<?

namespace Dna\Demomarket\Agent\Price;

use Bitrix\Main\Type\DateTime;
use Dna\Demomarket\Config;
use Dna\Demomarket\Price\Demomarket;
use Dna\Demomarket\Price\Dnahobby;
use Project\Tools\Utility\Cron;

class Export
{

    /**
     *
     */
    public static function demomarket()
    {
        Cron::agent(function () {
            Demomarket::process(0, 0);
        });
        return 'Dna\Demomarket\Agent\Price\Export::demomarket();';
    }

    /**
     *
     */
    public static function dnahobby()
    {
        Cron::agent(function () {
            Dnahobby::process(0, 0);
        });
        return 'Dna\Demomarket\Agent\Price\Export::dnahobby();';
    }

    /**
     *
     */
    public static function clear()
    {
        Cron::agent(function () {
            DeleteDirFilesEx(Config::PRICE_XLX_PATH);
        });
        return 'Dna\Demomarket\Agent\Price\Export::clear();';
    }

}
