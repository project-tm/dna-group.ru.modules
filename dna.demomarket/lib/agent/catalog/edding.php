<?

namespace Dna\Demomarket\Agent\Catalog;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use CIBlockElement;
use Dna\Demomarket\Config;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cron;

class Edding
{

    const UPLOAD_PATH_DEMOMARKET = Config::EXPORT_TMP_PATH . "edding-demomarket.csv";
    const UPLOAD_PATH_DNAHOBBY = Config::EXPORT_TMP_PATH . "edding-dnahobby.csv";
    const LIMIT = 500;

    /**
     * @return string
     */
    public static function demomarket()
    {
        Cron::agent(function () {
            $path = Application::getDocumentRoot() . self::UPLOAD_PATH_DEMOMARKET;
            CheckDirPath($path);
            Loader::includeModule('iblock');
            Loader::includeModule('catalog');
            $page = 1;
            if (($handle = fopen($path, "w")) !== false) {
                do {
                    $res = CIBlockElement::GetList(
                        ["SORT" => "ASC"],
                        [
                            'SECTION_ACTIVE' => 'Y',
                            "ACTIVE"         => "Y",
                            "IBLOCK_ID"      => Iblock::getByCodeOnly(Config::IBLOCK_DEMOMARKET),
                        ],
                        false,
                        [
                            'iNumPage'  => $page,
                            'nPageSize' => self::LIMIT,
                        ],
                        ['ID', "XML_ID", "IBLOCK_SECTION_ID", "CATALOG_GROUP_1"]
                    );
                    $res->NavStart(self::LIMIT);
                    while ($arItem = $res->Fetch()) {
                        fputcsv($handle, [
                                $arItem['XML_ID'],
                                'http://demomarket.ru/catalog/' . $arItem['IBLOCK_SECTION_ID'] . '/#' . $arItem['ID'],
                                $arItem['CATALOG_PRICE_1'],
                            ], ';');
                    }
                    $page++;
                } while ($res->NavPageCount && $res->NavPageCount != $res->NavPageNomer);
                fclose($handle);
            }
        });
        return 'Dna\Demomarket\Agent\Catalog\Edding::demomarket();';
    }

    /**
     * @return string
     */
    public static function dnahobby()
    {
        Cron::agent(function () {
            $path = Application::getDocumentRoot() . self::UPLOAD_PATH_DNAHOBBY;
            CheckDirPath($path);
            Loader::includeModule('iblock');
            Loader::includeModule('catalog');
            $page = 1;
            if (($handle = fopen($path, "w")) !== false) {
                do {
                    $res = CIBlockElement::GetList(
                        [],
                        [
                            'SECTION_ACTIVE' => 'Y',
                            'ACTIVE'         => 'Y',
                            'IBLOCK_ID'      => Iblock::getByCodeOnly(Config::IBLOCK_ELEMENT_DNAHOBBY),
                        ],
                        false,
                        [
                            'iNumPage'  => $page,
                            'nPageSize' => self::LIMIT,
                        ],
                        ['PROPERTY_VID', 'PROPERTY_CML2_LINK.DETAIL_PAGE_URL', 'CATALOG_GROUP_1']
                    );
                    $res->NavStart(self::LIMIT);
                    while ($arItem = $res->GetNext()) {
                        fputcsv($handle,
                            [
                                $arItem['PROPERTY_VID_VALUE'],
                                'https://dnahobby.ru' . str_replace('//', '/',
                                    $arItem['PROPERTY_CML2_LINK_DETAIL_PAGE_URL']),
                                $arItem['CATALOG_PRICE_1'],
                            ], ';');
                    }
                    $page++;
                } while ($res->NavPageCount && $res->NavPageCount != $res->NavPageNomer);
                fclose($handle);
            }
        });
        return 'Dna\Demomarket\Agent\Catalog\Edding::dnahobby();';
    }


}
