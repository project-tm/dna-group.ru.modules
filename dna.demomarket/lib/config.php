<?

namespace Dna\Demomarket;

class Config
{

    /* UPLOAD */
    const UPLOAD_PATH = '/home/d/dnadmdh/upload/';
    const UPLOAD_TMP_PATH = '/upload/tmp/';

    /* EXPORT */
    const EXPORT_TMP_PATH = '/upload/tmp/export/';


    /* IBLOCK */
    const IBLOCK_DISCOUNT = 'discounts';
    const IBLOCK_DEMOMARKET = 'catalog';
    const IBLOCK_DNAHOBBY = 'catalog_dna';
    const IBLOCK_ELEMENT_DNAHOBBY = 'DH_EXAMPLES';

    /* PRICE_XLX */
    const PRICE_XLX_PATH = '/upload/tmp/price';
    const PRICE_XLX_TIME_OUT = 60 * 60 * 24;

}
