<?

namespace Dna\D2320708\Agent\Price;

use Bitrix\Main\Type\DateTime;
use CUser;
use Dna\D2320708;
use Dna\D2320708\Config;
use Project\Tools\Utility\Cron;

class Export
{

    /**
     *
     */
    public static function update()
    {
        Cron::agent(function () {
            $res = CUser::GetList(($by = "personal_country"), ($order = "desc"),
                [
                    "LAST_LOGIN_1" => (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->add('-30 day')->toString(),
                    "ACTIVE"       => "Y",
                ],
                ['FIELDS' => ['ID', 'LAST_LOGIN_1']]);
            while ($arUser = $res->Fetch()) {
                if (D2320708\Price\Export::process(0, 0, $arUser['ID'])) {
                    break;
                }
            }
        });
        return 'Dna\D2320708\Agent\Price\Export::update();';
    }

    /**
     *
     */
    public static function clear()
    {
        Cron::agent(function () {
            DeleteDirFilesEx(Config::PRICE_XLX_PATH);
        });
        return 'Dna\D2320708\Agent\Price\Export::clear();';
    }

}
