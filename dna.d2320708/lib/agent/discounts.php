<?

namespace Dna\D2320708\Agent;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use CIBlockElement;
use CUser;
use Dna\D2320708\Config;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\Settings;

class Discounts
{

    const UPLOAD_PATH_DISCOUNT = Config::UPLOAD_PATH . "discount.csv";
    const UPDATED_DISCOUNTS_IDS = Config::UPLOAD_TMP_PATH . "updated_discounts_ids";

    /**
     * @return string
     */
    public static function import()
    {
        Cron::agent(function () {
            $hash = sha1_file(self::UPLOAD_PATH_DISCOUNT);
            if (Settings::get(__CLASS__) == $hash) {
                return;
            }

            Loader::IncludeModule("iblock");
            $iblock_id = Iblock::getByCodeOnly(Config::IBLOCK_DISCOUNT);
            $items = [];

            $row = 1;
            $row_affected = 0;
            if (($handle = fopen(self::UPLOAD_PATH_DISCOUNT, "r")) !== false) {
                $el = new CIBlockElement;
                while (($data = fgetcsv($handle, 0, ";")) !== false) {
                    if ($row > 1) {

                        foreach ($data as $key => $val) {
                            $data[$key] = trim($data[$key]);
                            $data[$key] = str_replace('"', '', $data[$key]);
                        }
                        $tmpArr = explode('/', $data[1]);
                        $userRes = CUser::GetList(($by = "ID"), ($order = "DESC"), ["=UF_INN" => $tmpArr[0]],
                            ["FIELDS" => ["ID"]]);
                        if ($arUser = $userRes->Fetch()) {

                            $arFields = [
                                "NAME"            => $data[0],
                                "IBLOCK_ID"       => $iblock_id,
                                "ACTIVE"          => "Y",
                                "PROPERTY_VALUES" => [
                                    "INN"       => $tmpArr[0],
                                    "KPP"       => $tmpArr[1],
                                    "DISCOUNT"  => $data[3],
                                    "FOLDER_1C" => $data[2],
                                    "USER"      => $arUser["ID"],
                                    "PRICETYPE" => $data[4],
                                ],
                            ];

                            $rsElem = CIBlockElement::GetList(["ID" => "ASC"], [
                                'IBLOCK_ID'          => $iblock_id,
                                '=PROPERTY_INN'       => $tmpArr[0],
                                '=PROPERTY_KPP'       => $tmpArr[1],
                                "=PROPERTY_FOLDER_1C" => $data[2],
                            ], false, false, ['ID']);
                            if ($arElem = $rsElem->GetNext()) {
                                $ELEM_ID = $arElem["ID"];
                                $el->Update($ELEM_ID, $arFields);
                            } else {
                                $ELEM_ID = $el->Add($arFields);
                            }
                            $items[$ELEM_ID] = $ELEM_ID;

                            $groups = CUser::GetUserGroup($arUser["ID"]);
                            $groups[] = Config::USER_DIALER_1;

                            $oUser = new CUser;
                            $oUser->Update($arUser["ID"], ["UF_IN1C" => true, "GROUP_ID" => $groups]);

                        } else {
                            $row = $row + 1;
                            continue;
                        }

                        $row_affected = $row_affected + 1;
                    }
                    $row = $row + 1;
                }
                fclose($handle);

                $rsElem = CIBlockElement::GetList([], ['IBLOCK_ID' => $iblock_id, '!ID' => $items, 'ACTIVE' => 'Y'], false,
                    false, ['ID', 'NAME']);
                while ($arElem = $rsElem->GetNext()) {
                    $el->Update($arElem["ID"], ["ACTIVE" => "N"]);
                }
                Settings::set(__CLASS__, $hash);
            }
        });
        return 'Dna\D2320708\Agent\Discounts::import();';
    }

}
