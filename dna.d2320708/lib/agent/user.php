<?

namespace Dna\D2320708\Agent;

use CUser;
use Dna\D2320708\Config;
use Project\Tools\Utility\Cron;

class User
{

    /**
     *
     */
    public static function export()
    {
        Cron::agent(function () {
            set_time_limit(0);
            $handle = fopen(Config::UPLOAD_PATH . "users.csv", "w");

            $head = [
                "ID Bitrix",
                "NAME",
                "SECOND NAME",
                "LAST_NAME",
                "EMAIL",
                "PHONE",
                "COMPANY",
                "REGION",
                "ADDRESS",
                "INN",
                "KPP",
                "RS",
                "KS",
                "BIC",
                "BANK",
            ];
            fputcsv($handle, $head, ';', '"');

            $userRes = CUser::GetList(($by = "ID"), ($order = "DESC"),
                ["UF_IN1C" => false, "GROUPS_ID" => [Config::USER_EXPORT_GROUP_ID]], [
                    "FIELDS" => [
                        "ID",
                        "NAME",
                        "SECOND_NAME",
                        "LAST_NAME",
                        "EMAIL",
                        "PERSONAL_PHONE",
                        "WORK_COMPANY",
                        "WORK_STATE",
                        "WORK_STREET",
                    ],
                    "SELECT" => ["UF_INN", "UF_KPP", "UF_RS", "UF_CS", "UF_BIC", "UF_BANK"],
                ]);
            while ($arUser = $userRes->Fetch()) {
                Cron::next();
                foreach ($arUser as $key => $val) {
                    $arUser[$key] = mb_convert_encoding($val, 'Windows-1251', 'utf-8');
                }
                fputcsv($handle, $arUser, ';', '"');
            }
            fclose($handle);

        });
        return 'Dna\D2320708\Agent\User::export();';
    }

}
