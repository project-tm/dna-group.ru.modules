<?

namespace Dna\D2320708\Agent\Catalog;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use CIBlockElement;
use Dna\D2320708\Config;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cron;

class Edding
{

    const UPLOAD_PATH_OPER = Config::EXPORT_TMP_PATH . "edding.csv";
    const LIMIT = 500;

    /**
     * @return string
     */
    public static function export()
    {
        Cron::agent(function () {
            $path = Application::getDocumentRoot() . self::UPLOAD_PATH_OPER;
            CheckDirPath($path);
            Loader::includeModule('iblock');
            Loader::includeModule('catalog');
            $page = 1;
            if (($handle = fopen($path, "w")) !== false) {
                do {
                    $res = CIBlockElement::GetList(
                        [],
                        [
                            'SECTION_ACTIVE' => 'Y',
                            'ACTIVE'         => 'Y',
                            'IBLOCK_ID'      => Iblock::getByCodeOnly(Config::IBLOCK_ELEMENT_DNA),
                        ],
                        false,
                        [
                            'iNumPage'  => $page,
                            'nPageSize' => self::LIMIT,
                        ],
                        ['PROPERTY_VID', 'PROPERTY_CML2_LINK.DETAIL_PAGE_URL', 'CATALOG_GROUP_1']
                    );
                    $res->NavStart(self::LIMIT);
                    while ($arItem = $res->GetNext()) {
                        fputcsv($handle,
                            [
                                $arItem['PROPERTY_VID_VALUE'],
                                Config::URL . str_replace('//', '/', $arItem['PROPERTY_CML2_LINK_DETAIL_PAGE_URL']),
                                $arItem['CATALOG_PRICE_1'],
                            ], ';');
                    }
                    $page++;
                } while ($res->NavPageCount && $res->NavPageCount != $res->NavPageNomer);
                fclose($handle);
            }
        });
        return 'Dna\D2320708\Agent\Catalog\Edding::export();';
    }

}
