<?

namespace Dna\D2320708\Price;

use Bitrix\Main\Loader;
use CFile;
use CIBlockElement;
use CIBlockSection;
use Dna\D2320708\Config;
use Dna\D2320708\Discount;
use PhpOffice\PhpSpreadsheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\User;

class Export
{

    /**
     * @param $userId
     */
    static private function before($userId)
    {
        if ($userId) {
            User::authorize($userId);
        }
    }

    /**
     * @param $userId
     */
    static private function after($userId)
    {
        if ($userId) {
            User::logout();
        }
    }

    /**
     *
     */
    static private function headerReadyXls()
    {
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=2320708_price.xlsx");
    }

    /**
     * @param $sectionId
     * @param $brandId
     * @param $userId
     *
     * @return string
     * @throws \Exception
     */
    static private function getPath($sectionId, $brandId, $userId)
    {
        return $_SERVER["DOCUMENT_ROOT"] . Config::PRICE_XLX_PATH . '/price_' . implode('-',
                [sha1(__FILE__), $sectionId, $brandId ? sha1($brandId) : 0, Discount::getKey($userId)]) . '.txt';
    }

    /**
     * @param      $sectionId
     * @param      $brandId
     * @param null $userId
     *
     * @throws \Exception
     */
    static public function donwload($sectionId, $brandId, $userId = null)
    {
        self::before($userId);
        $fileOut = self::getPath($sectionId, $brandId, $userId);
        if (file_exists($fileOut) && (filemtime($fileOut) + Config::PRICE_XLX_TIME_OUT) > time()) {
            self::after($userId);
            self::headerReadyXls();
            readfile($fileOut);
            exit;
        }
        self::headerReadyXls();
        self::generate($sectionId, $brandId, $fileOut);
        self::after($userId);
        readfile($fileOut);
    }

    /**
     * @param      $sectionId
     * @param      $brandId
     * @param null $userId
     *
     * @return bool
     * @throws \Exception
     */
    static public function process($sectionId, $brandId, $userId = null)
    {
        self::before($userId);
        $fileOut = self::getPath($sectionId, $brandId, $userId);
//        preDebug(__FUNCTION__, $userId, $fileOut);
        self::after($userId);
        if (file_exists($fileOut) && (filemtime($fileOut) + Config::PRICE_XLX_TIME_OUT) > time()) {
            self::after($userId);
            return false;
        }
        self::generate($sectionId, $brandId, $fileOut);
        self::after($userId);
        return true;
    }

    /**
     * @param $sectionId
     * @param $brandId
     *
     * @param $fileOut
     *
     * @throws \Bitrix\Main\LoaderException
     */
    static private function generate($sectionId, $brandId, $fileOut)
    {
        Cron::next();
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
        $secFilter = ["IBLOCK_ID" => 48, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y"];

        if ($sectionId > 0) {
            $rsSections = CIBlockSection::GetList(['LEFT_MARGIN' => 'ASC'], ["ID" => $sectionId]);
            if ($arSection = $rsSections->Fetch()) {
                if ($arSection["DEPTH_LEVEL"] == 1) {
                    $secFilter["SECTION_ID"] = $sectionId;
                } else {
                    $secFilter["ID"] = $sectionId;
                }
            }
        }

        $first_level = '';
        $second_level = '';
        $rsSections = CIBlockSection::GetList(['LEFT_MARGIN' => 'ASC'], $secFilter);


        require $_SERVER["DOCUMENT_ROOT"] . '/PhpSpreadsheet-develop/vendor/autoload.php';


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Изображение');
        $sheet->setCellValue('B1', 'Артикул');
        $sheet->setCellValue('C1', 'Название');
        $sheet->setCellValue('D1', 'Характеристика');
        $sheet->setCellValue('E1', 'Значение');
        $sheet->setCellValue('F1', 'Производитель');
        $sheet->setCellValue('G1', 'Тип товара');
        $sheet->setCellValue('H1', 'Цена');

        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(15);
        $sheet->getColumnDimension('C')->setWidth(40);
        $sheet->getColumnDimension('D')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(30);
        $sheet->getColumnDimension('H')->setWidth(15);

        $i = 2;

        $arrFilter = ["IBLOCK_ID" => Iblock::getByCodeOnly(Config::IBLOCK_ELEMENT_DNA), "ACTIVE" => "Y"];

        while ($arSection = $rsSections->Fetch()) {
            $arrFilter["SECTION_ID"] = $arSection["ID"];
            if ($sectionId > 0) {
                $arrFilter["INCLUDE_SUBSECTIONS"] = "Y";
            }
            if (!empty($brandId)) {
                $arrFilter["=PROPERTY_PROIZ"] = htmlspecialcharsEx($brandId);
            }

            if ($arSection["DEPTH_LEVEL"] == 1) {
                $first_level = $arSection["NAME"];
                $first_level_shown = false;
            } elseif ($arSection["DEPTH_LEVEL"] > 1) {
                $second_level = $arSection["NAME"];
                $second_level_shown = false;
            }

            AddMessage2Log($arrFilter);

            $res = CIBlockElement::GetList(["SORT" => "ASC"], $arrFilter, false, false, [
                "NAME",
                "DETAIL_PICTURE",
                "PROPERTY_VID",
                "CATALOG_GROUP_1",
                "PROPERTY_PROIZ",
                "PROPERTY_ATTRIBUTE",
                "PROPERTY_ATTRVALUE",
                "PROPERTY_TOVNAME",
            ]);
            while ($ar_fields = $res->GetNext()) {
                Cron::next();
                if (!$first_level_shown) {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFill()
                        ->setFillType(PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFCCCCCC');
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFont()->setBold(true);
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFont()->setSize(18);

                    $sheet->mergeCells('A' . $i . ':G' . $i);
                    $sheet->setCellValue('A' . $i, $first_level);
                    $i++;
                    $first_level_shown = true;
                }
                if (!$second_level_shown) {
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFill()
                        ->setFillType(PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFF1F1F1');
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFont()->setBold(true);
                    $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':H' . $i)->getFont()->setSize(14);
                    $sheet->mergeCells('A' . $i . ':G' . $i);
                    $sheet->setCellValue('A' . $i, $second_level);
                    $i++;
                    $second_level_shown = true;
                }

                if ($ar_fields["DETAIL_PICTURE"] != '') {
                    $file = CFile::ResizeImageGet($ar_fields["DETAIL_PICTURE"], ['width' => 100, 'height' => 75],
                        BX_RESIZE_IMAGE_PROPORTIONAL, true);

                    if (file_exists($_SERVER["DOCUMENT_ROOT"] . $file['src'])) {
                        $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(60);
                        $img = new PhpSpreadsheet\Worksheet\Drawing();
                        $img->setName($ar_fields["PROPERTY_TOVNAME_VALUE"]);
                        $img->setPath($_SERVER["DOCUMENT_ROOT"] . $file['src']);
                        $img->setCoordinates('A' . $i);
                        $img->setOffsetX(0);
                        $img->setOffsetY(0);
                        $img->setWorksheet($spreadsheet->getActiveSheet());
                    }
                }
                $spreadsheet->getActiveSheet()->getStyle('A' . $i . ':G' . $i)->getAlignment()->setVertical(PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->setCellValue('B' . $i, $ar_fields["NAME"]);
                $sheet->setCellValue('C' . $i, $ar_fields["PROPERTY_TOVNAME_VALUE"]);
                $sheet->setCellValue('D' . $i, $ar_fields["PROPERTY_ATTRIBUTE_VALUE"]);
                $sheet->setCellValue('E' . $i, $ar_fields["PROPERTY_ATTRVALUE_VALUE"]);
                $sheet->setCellValue('F' . $i, $ar_fields["PROPERTY_PROIZ_VALUE"]);
                $sheet->setCellValue('G' . $i, $second_level);
                $sheet->setCellValue('H' . $i, $ar_fields["CATALOG_PRICE_1"] . ' руб.');
                $i++;
            }
        }

        $spreadsheet->getActiveSheet()->setAutoFilter(
            $spreadsheet->getActiveSheet()
                ->calculateWorksheetDimension()
        );

        $writer = new Xlsx($spreadsheet);

        CheckDirPath($fileOut);
        $writer->save($fileOut);
        Cron::stop();
    }

}
