<?

namespace Dna\D2320708;

class Config
{

    const URL = 'http://2320708.ru';

    /* UPLOAD */
    const UPLOAD_PATH = '/var/www/www-root/data/www/upload/';
    const UPLOAD_TMP_PATH = '/upload/tmp/';

    /* EXPORT */
    const EXPORT_TMP_PATH = '/upload/tmp/export/';

    /* USER */
    /* Дилеры 1 группы */
    const USER_DIALER_1 = 22;
    /* Зарегистрировавшиеся */
    const USER_EXPORT_GROUP_ID = 27;

    /* IBLOCK */
    const IBLOCK_DISCOUNT = 'discounts';
    const IBLOCK_SHOP_DNA = 'shop_dna';
    const IBLOCK_ELEMENT_DNA = 'element_dna';

    /* PRICE_XLX */
    const PRICE_XLX_PATH = '/upload/tmp/price';
    const PRICE_XLX_TIME_OUT = 60 * 60 * 24;

}
