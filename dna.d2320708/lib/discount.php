<?

namespace Dna\D2320708;

use Bitrix\Main\Loader;
use CIBlockElement;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cache;

class Discount
{

    /**
     * @param $userId
     *
     * @return null
     * @throws \Exception
     */
    public static function getKey($userId)
    {
        if (empty($userId)) {
            return $userId;
        }
        return sha1(implode('-', Cache::getCached(function () use ($userId) {
            Loader::includeModule('iblock');
            $res = CIBlockElement::GetList(
                ['PROPERTY_FOLDER_1C' => 'ASC', 'PROPERTY_DISCOUNT' => 'ASC', 'PROPERTY_PRICETYPE' => 'ASC'],
                [
                    'ACTIVE'        => 'Y',
                    'IBLOCK_ID'     => Iblock::getByCodeOnly(Config::IBLOCK_DISCOUNT),
                    'PROPERTY_USER' => $userId,
                ],
                false,
                false,
                ['PROPERTY_USER', 'PROPERTY_DISCOUNT', 'PROPERTY_PRICETYPE', 'PROPERTY_FOLDER_1C']
            );
            $arDiscount = [];
            while ($arItem = $res->Fetch()) {
                $arDiscount[] = $arItem['PROPERTY_FOLDER_1C'];
                $arDiscount[] = $arItem['PROPERTY_DISCOUNT_VALUE'];
                $arDiscount[] = $arItem['PROPERTY_PRICETYPE_VALUE'];
            }
            return $arDiscount;
        }, ['PARAM' => $userId, 'MEMORY' => true])));
    }

}
