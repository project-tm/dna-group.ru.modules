<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Project\Tools\Modules;

if(!Loader::includeModule('jerff.core')) {
    return;
}

Loc::loadMessages(__FILE__);

class dna_d2320708 extends CModule
{

    public $MODULE_ID = 'dna.d2320708';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, 'DNA_D2320708');
        $this->MODULE_NAME = Loc::getMessage('DNA_D2320708_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DNA_D2320708_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('DNA_D2320708_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DNA_D2320708_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    /*
     * InstallEvent
     */

    public function InstallEvent()
    {
        $this->registerEventHandler('main', 'OnPageStart', '\Dna\D2320708\Event\Page', 'OnPageStart');
    }

    public function UnInstallEvent()
    {
        $this->unRegisterEventHandler('main', 'OnPageStart', '\Dna\D2320708\Event\Page', 'OnPageStart');
    }

    public function InstallAgent()
    {

        $this->InstallAgentTime('Dna\D2320708\Agent\Oper::import();', 60);
        /* User */
        $this->InstallAgentTime('Dna\D2320708\Agent\User::export();', 15 * 60);
        $this->InstallAgentTime('Dna\D2320708\Agent\Discounts::import();', 60 * 60);

        /* Catalog */
        $this->AddAgent(
            'Dna\D2320708\Agent\Catalog\Edding::export();',
            'N',
            60 * 60 * 4,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString()
        );

        /* Price */
        $this->InstallAgentTime('Dna\D2320708\Agent\Price\Import::update();', 30);
        $this->InstallAgentTime('Dna\D2320708\Agent\Price\Export::update();', 5 * 60);
        $this->AddAgent(
            'Dna\D2320708\Agent\Price\Export::clear();',
            'Y',
            60 * 60 * 24,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->add('1 day')->toString()
        );
    }

    public function InstallAgentTime($name, $interval)
    {
        $this->AddAgent(
            $name,
            'N',
            $interval,
            (new DateTime())->toString(),
            'Y',
            (new DateTime())->toString()
        );
    }

    public function UnInstallAgent()
    {
        CAgent::RemoveAgent('Dna\D2320708\Agent\Oper::import();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\D2320708\Agent\User::export();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\D2320708\Agent\Discounts::import();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\D2320708\Agent\Catalog\Edding::export();', $this->MODULE_ID);

        CAgent::RemoveAgent('Dna\D2320708\Agent\Price\Import::update();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\D2320708\Agent\Price\Export::update();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\D2320708\Agent\Price\Export::clear();', $this->MODULE_ID);
        return true;
    }

}
