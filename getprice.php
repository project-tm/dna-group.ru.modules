<?
define("NEED_AUTH", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);

use Bitrix\Main\Loader;
use Dna\D2320708\Price\Export;
if(Loader::includeModule('dna.d2320708')) {
    Export::donwload((int)$_GET['sec_id'] ?? 0, trim($_GET['prod'] ?? 0 ));
    exit;
}