<?php

use Bitrix\Main\Loader;

$id = 'jerff.core';
if (!Loader::includeModule($id) and $Module = CModule::CreateModuleObject($id)) {
    if ($Module->DoInstall() === false) {
        global $APPLICATION, $errorMessage;
        $errorMessage = GetMessage("MOD_INSTALL_ERROR", ["#CODE#" => $id]);
        if ($e = $APPLICATION->GetException()) {
            throw new Exception($e->GetString());;
        }
    }
}
