<?

namespace Dna\Kotmegapolis\Model;

use Dna\Kotmegapolis\Database;
use Project\Tools\Utility\Cache;

class Edding
{

    static public function seach($ID_KM, $VID, $ARTNUM)
    {
        return Cache::getCached(function () use ($ID_KM, $VID, $ARTNUM) {
            $arResult = [];
            $res = Database\EddingPriceTable::getlist([
                'select' => [
                    'SITE',
                    'URL',
                    'PRICE',
                ],
                'filter' => [
                    'LOGIC' => 'OR',
                    [
                        'SITE' => 'kotmegapolis.ru',
                        '=CODE' => $ID_KM,
                    ],
                    [
                        'SITE' => ['2320708.ru', 'dnahobby.ru'],
                        '=CODE' => $VID,
                    ],
                    [
                        'SITE' => 'demomarket.ru',
                        'CODE' => $ARTNUM . '%',
                    ],
                ],
            ]);
            while ($arItem = $res->Fetch()) {
                $arResult[$arItem['SITE']] = [
                    'URL'   => $arItem['URL'],
                    'PRICE' => $arItem['PRICE'],
                ];
            }
            return $arResult;
        }, ['PARAM' => [$ID_KM, $VID, $ARTNUM], 'CACHE_TIME' => 60 * 60 * 24]);
    }


}
