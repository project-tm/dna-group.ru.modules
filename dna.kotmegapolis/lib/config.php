<?

namespace Dna\Kotmegapolis;

class Config
{

    /* UPLOAD */
    const UPLOAD_PATH = '/home/d/dnadmdh/upload/';
    const UPLOAD_TMP_PATH = '/upload/tmp/';

    /* EXPORT */
    const EXPORT_TMP_PATH = '/upload/tmp/export/';

    /* USER */
    /* Дилеры 1 группы */
    const USER_DIALER_1 = 22;
    /* Зарегистрировавшиеся */
    const USER_EXPORT_GROUP_ID = 27;

    /* IBLOCK */
    const IBLOCK_DISCOUNT = 'discounts';
    const IBLOCK_KOTMEGAPOLIS = 'catalog';
    const IBLOCK_ELEMENT_ROTRINGSHOP = 'rs_catalog_element';
    const IBLOCK_ELEMENT_MARKERLAND = 'ML_EXAMPLES';

    /* PRICE_XLX */
    const PRICE_XLX_PATH = '/upload/tmp/price';
    const PRICE_XLX_TIME_OUT = 60 * 60 * 24;

    /* CRON */
    const CRON_TIME_OUT = 60 * 5;

    /* EDIING */
    const EDIING_IMPORT_MAP = [
        '2320708.ru'       => 'https://2320708.ru/upload/tmp/export/edding.csv',
        'kot-megapolis.ru' => 'http://kot-megapolis.ru/upload/tmp/export/edding.csv',
        'dnahobby.ru'      => 'https://dnahobby.ru/upload/tmp/export/edding-dnahobby.csv',
//        'demomarket.ru'    => 'http://demomarket.ru/upload/tmp/export/edding-demomarket.csv',
        'demomarket.ru'    => 'https://dnahobby.ru/upload/tmp/export/edding-demomarket.csv',
    ];

}
