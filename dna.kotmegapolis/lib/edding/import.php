<?

namespace Dna\Kotmegapolis\Edding;

use Bitrix\Main\Application;
use Dna\Kotmegapolis\Config;
use Dna\Kotmegapolis\Database;
use Project\Tools\Utility\Settings;

class Import
{

    const PATH = Config::UPLOAD_TMP_PATH . 'import/';

    static private function getPath($site)
    {
        return Application::getDocumentRoot() . self::PATH . $site . '.csv';
    }

    static public function donwoload()
    {
        foreach (Config::EDIING_IMPORT_MAP as $site => $url) {
            $pathFile = self::getPath($site);
            CheckDirPath($pathFile);
            file_put_contents($pathFile, file_get_contents($url));
        }
    }

    static public function process()
    {
        foreach (Config::EDIING_IMPORT_MAP as $site => $url) {
            $pathFile = self::getPath($site);
            if (!file_exists($pathFile)) {
                continue;
            }
            $hash = sha1_file($pathFile);
            $pathKey = __CLASS__ . $site;
            if (Settings::get($pathKey) == $hash) {
                continue;
            }
            Application::getConnection()->StartTransaction();

            if (($handle = fopen($pathFile, "r")) !== false) {
                Database\EddingPriceTable::update(
                    ['SITE' => $site],
                    ['ACTIVE' => 'N']
                );
                Database\EddingPriceTable::startBigData();
                while (($data = fgetcsv($handle, 0, ";")) !== false) {
                    if (count($data) > 3) {
                        pre($data);
                    }
                    Database\EddingPriceTable::add([
                        'SITE'  => $site,
                        'CODE'  => $data[0],
                        'URL'   => $data[1],
                        'PRICE' => $data[2],
                    ]);
                }
                Database\EddingPriceTable::compileBigData();
            }
            Database\EddingPriceTable::delete(
                ['SITE' => $site, 'ACTIVE' => 'N']
            );
            Application::getConnection()->commitTransaction();
            Settings::set($pathKey, $hash);
            break;
        }
    }


}
