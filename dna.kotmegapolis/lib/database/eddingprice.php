<?php

namespace Dna\Kotmegapolis\Database;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Project\Tools\Orm\TraitList\BigData;
use Project\Tools\Orm\TraitList\Query;

class EddingPriceTable extends DataManager
{

    use Query;
    use BigData;
    const BIGDATA_LIMIT = 100;

    public static function getTableName()
    {
        return 'dna_kotmegapolis_edding_price';
    }

    public static function getMap()
    {
        return [
            'ID'     => new Entity\IntegerField('ID', [
                'primary'  => true,
                'required' => true,
            ]),
            'ACTIVE' => new Entity\EnumField('ACTIVE', [
                'primary'  => true,
                'values' => [
                    "Y",
                    "N",
                ],
            ]),
            'SITE'   => new Entity\StringField('SITE', array(
                'primary'  => true,
            )),
            'CODE'   => new Entity\StringField('CODE'),
            'URL'    => new Entity\StringField('URL'),
            'PRICE'  => new Entity\StringField('PRICE'),
        ];
    }

}