<?

namespace Dna\Kotmegapolis\Agent\Price;

use Bitrix\Main\Type\DateTime;
use Dna\Kotmegapolis\Config;
use Dna\Kotmegapolis\Price\Markerland;
use Dna\Kotmegapolis\Price\Rotringshop;
use Project\Tools\Utility\Cron;

class Export
{

    /**
     *
     */
    public static function markerland()
    {
        Cron::agent(function () {
            Markerland::process(0, 0);
        });
        return 'Dna\Kotmegapolis\Agent\Price\Export::markerland();';
    }

    /**
     *
     */
    public static function rotringshop()
    {
        Cron::agent(function () {
            Rotringshop::process(0, 0);
        });
        return 'Dna\Kotmegapolis\Agent\Price\Export::rotringshop();';
    }

    /**
     *
     */
    public static function clear()
    {
        Cron::agent(function () {
            DeleteDirFilesEx(Config::PRICE_XLX_PATH);
        });
        return 'Dna\Kotmegapolis\Agent\Price\Export::clear();';
    }

}
