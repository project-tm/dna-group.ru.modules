<?

namespace Dna\Kotmegapolis\Agent;

use Bitrix\Main\Loader;
use CCatalogProduct;
use CCatalogStoreProduct;
use CIBlockElement;
use CPrice;
use Dna\Kotmegapolis\Config;
use Project\Tools\Utility\Cron;
use Project\Tools\Utility\Settings;

class Oper
{

    const UPLOAD_PATH_OPER = Config::UPLOAD_PATH . "oper.csv";

    /**
     * @return string
     */
    public static function markerland()
    {
        Cron::agent(function () {
            self::import(Config::IBLOCK_ELEMENT_MARKERLAND);
        });
        return 'Dna\Kotmegapolis\Agent\Oper::markerland();';
    }

    /**
     * @return string
     */
    public static function rotringshop()
    {
        Cron::agent(function () {
            self::import(Iblock::getByCodeOnly(Config::IBLOCK_ELEMENT_ROTRINGSHOP));
        });
        return 'Dna\Kotmegapolis\Agent\Oper::rotringshop();';
    }

    /**
     * @param $ibid
     *
     * @throws \Bitrix\Main\LoaderException
     */
    private static function import($ibid)
    {
        $hash = sha1_file(self::UPLOAD_PATH_OPER);
        if (Settings::get(__CLASS__) == $hash) {
            return;
        }

        Loader::IncludeModule("iblock");
        Loader::IncludeModule("catalog");


        $sku_iblock_id = $ibid;

        AddMessage2Log('Старт опертаивной выгрузки');

        $row = 1;
        $row_affected = 0;
        if (($handle = fopen(self::UPLOAD_PATH_OPER, "r")) !== false) {
            AddMessage2Log('Файл открылся');
            while (($data = fgetcsv($handle, 0, ";")) !== false) {
                /*if($row>$step*1000) break;
                if($row<=($step-1)*1000)
                {
                    $row++;
                    continue;
                }*/
                if ($row > 1) {

                    foreach ($data as $key => $val) {
                        $data[$key] = mb_convert_encoding($val, 'utf-8', 'Windows-1251');
                        $data[$key] = trim($data[$key]);
                        $data[$key] = str_replace('"', '', $data[$key]);
                    }

                    AddMessage2Log($data);

                    $arFields = [
                        "NAME"      => $data[0],
                        "IBLOCK_ID" => $sku_iblock_id,
                    ];

                    $el = new CIBlockElement;

                    $PRODUCT_ID = 0;

                    $rsElem = CIBlockElement::GetList([], ['IBLOCK_ID' => $sku_iblock_id, 'NAME' => $data[0]], false,
                        false, ['ID']);
                    if ($arElem = $rsElem->Fetch()) {

                        $PRODUCT_ID = $arElem["ID"];
                        $el->Update($PRODUCT_ID, $arFields);

                        if ($data[5] != '') {
                            $arFields['SPECPRICE'] = $data[5];
                        }
                        if ($data[6] != '') {
                            $arFields['TMPPRICE'] = $data[6];
                        }
                        if ($data[3] != '') {
                            $arFields['WODISCOUNTPRICE'] = $data[2];
                        } else {
                            $arFields['WODISCOUNTPRICE'] = '';
                        }
                        if ($arFields) {
                            CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, false, $arFields);
                        }


                        //Устанавливаем вес
                        $db_res = CCatalogProduct::GetList([], ["ID" => $PRODUCT_ID]);
                        $catArr = ["ID" => $PRODUCT_ID, "QUANTITY" => $data[1]];
                        if (($ar_res = $db_res->Fetch())) {
                            CCatalogProduct::Update($PRODUCT_ID, $catArr);
                        } else {
                            CCatalogProduct::Add($catArr);
                        }

                        //Устанавливаем цену
                        $db_res = CPrice::GetList([], ["PRODUCT_ID" => $PRODUCT_ID, "CATALOG_GROUP_ID" => 1]);
                        $priceArr = [
                            "PRODUCT_ID"       => $PRODUCT_ID,
                            "CATALOG_GROUP_ID" => 1,
                            "PRICE"            => $data[3] != '' ? str_replace(',', '.', $data[3]) : str_replace(',',
                                '.', $data[2]),
                            "CURRENCY"         => "RUB",
                        ];
                        if ($ar_res = $db_res->Fetch()) {
                            //AddMessage2Log($priceArr);
                            CPrice::Update($ar_res["ID"], $priceArr);
                        } else {
                            CPrice::Add($priceArr);
                        }

                        //Устанавливаем остаток на складе в МСК
                        $arAmoutn = [
                            1 => 7,
                            2 => 8,
                            3 => 9,
                        ];
                        $rsStore = CCatalogStoreProduct::GetList([], ['PRODUCT_ID' => $PRODUCT_ID, 'STORE_ID' => 1],
                            false, false, ['ID']);
                        while ($arStore = $rsStore->Fetch()) {
                            if (isset($arAmoutn[$arStore['STORE_ID']])) {
                                CCatalogStoreProduct::Update($arStore['ID'],
                                    ["AMOUNT" => $data[$arAmoutn[$arStore['STORE_ID']]]]);
                                unset($arAmoutn[$arStore['STORE_ID']]);
                            }
                        }
                        foreach ($arAmoutn[$arStore['STORE_ID']] as $key => $value) {
                            CCatalogStoreProduct::Add([
                                "PRODUCT_ID" => $PRODUCT_ID,
                                "STORE_ID"   => $key,
                                "AMOUNT"     => $data[$value],
                            ]);
                        }
                    }
                    $row_affected = $row_affected + 1;
                }
                $row++;

                //if($row>100) break;
            }

            fclose($handle);
            Settings::set(__CLASS__, $hash);
            //echo '<br/><br/>'.$row_affected.' элементов обработано.<br/>';
            //echo '<a href="import_oper.php?step='.($step+1).'" id="nextstep">Перейти к следующему шагу</a>';
        }
    }

}
