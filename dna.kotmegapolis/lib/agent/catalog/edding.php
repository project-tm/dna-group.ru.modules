<?

namespace Dna\Kotmegapolis\Agent\Catalog;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use CIBlockElement;
use Dna\Kotmegapolis\Config;
use Dna\Kotmegapolis\Edding\Import;
use Project\Tools\Iblock\Iblock;
use Project\Tools\Utility\Cron;

class Edding
{

    const UPLOAD_PATH_OPER = Config::EXPORT_TMP_PATH . "edding.csv";
    const LIMIT = 500;

    /**
     * @return string
     */
    public static function export()
    {
        Cron::agent(function () {
            $path = Application::getDocumentRoot() . self::UPLOAD_PATH_OPER;
            CheckDirPath($path);
            Loader::includeModule('iblock');
            Loader::includeModule('catalog');
            $page = 1;
            if (($handle = fopen($path, "w")) !== false) {
                do {
                    $res = CIBlockElement::GetList(
                        [],
                        [
                            'SECTION_ACTIVE' => 'Y',
                            'ACTIVE'         => 'Y',
                            'IBLOCK_ID'      => Iblock::getByCodeOnly(Config::IBLOCK_KOTMEGAPOLIS),
                        ],
                        false,
                        [
                            'iNumPage'  => $page,
                            'nPageSize' => self::LIMIT,
                        ],
                        ['ID', 'IBLOCK_SECTION_ID', 'CATALOG_GROUP_1']
                    );
                    $res->NavStart(self::LIMIT);
                    while ($arItem = $res->Fetch()) {
                        fputcsv($handle,
                            [
                                $arItem['ID'],
                                'http://kotmegapolis.ru/catalog/' . $arItem['IBLOCK_SECTION_ID'] . '/' . $arItem['ID'] . '/',
                                $arItem['CATALOG_PRICE_1'],
                            ], ';');
                    }
                    $page++;
                } while ($res->NavPageCount && $res->NavPageCount != $res->NavPageNomer);
                fclose($handle);
            }
        });
        return 'Dna\Kotmegapolis\Agent\Catalog\Edding::export();';
    }

    public static function import()
    {
        Cron::agent(function () {
            Import::process();
        });
        return 'Dna\Kotmegapolis\Agent\Catalog\Edding::import();';
    }

    public static function donwload()
    {
        Cron::agent(function () {
            Import::donwoload();
        });
        return 'Dna\Kotmegapolis\Agent\Catalog\Edding::donwload();';
    }

}
