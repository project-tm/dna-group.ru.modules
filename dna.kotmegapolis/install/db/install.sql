CREATE TABLE IF NOT EXISTS `dna_kotmegapolis_edding_price`
(
  `ID`     int(10) UNSIGNED                             NOT NULL AUTO_INCREMENT,
  `ACTIVE` enum ('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SITE`   varchar(50) COLLATE utf8_unicode_ci          NOT NULL,
  `CODE`   varchar(150) COLLATE utf8_unicode_ci         NOT NULL,
  `URL`    varchar(260) COLLATE utf8_unicode_ci         NOT NULL,
  `PRICE`  varchar(20) COLLATE utf8_unicode_ci          NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SITE` (`SITE`, `CODE`),
  KEY `ACTIVE` (`ACTIVE`, `SITE`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;