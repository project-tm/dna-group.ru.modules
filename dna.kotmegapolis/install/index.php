<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Project\Tools\Modules;

if (!Loader::includeModule('jerff.core')) {
    return;
}

Loc::loadMessages(__FILE__);

class dna_kotmegapolis extends CModule
{

    public $MODULE_ID = 'dna.kotmegapolis';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, 'DNA_KOTMEGAPOLIS');
        $this->MODULE_NAME = Loc::getMessage('DNA_KOTMEGAPOLIS_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DNA_KOTMEGAPOLIS_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('DNA_KOTMEGAPOLIS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DNA_KOTMEGAPOLIS_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    public function InstallDb()
    {
        global $DB;
        $this->UnInstallDb();
        $DB->RunSQLBatch(__DIR__ . '/db/install.sql');
    }

    public function UnInstallDb()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');
    }

    /*
     * InstallEvent
     */

    public function InstallEvent()
    {
        $this->registerEventHandler('main', 'OnPageStart', '\Dna\Kotmegapolis\Event\Page', 'OnPageStart');
    }

    public function UnInstallEvent()
    {
        $this->unRegisterEventHandler('main', 'OnPageStart', '\Dna\Kotmegapolis\Event\Page', 'OnPageStart');
    }

    public function InstallAgent()
    {
        /* User */
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Oper::markerland();', 60);
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Oper::rotringshop();', 60);

        /* Catalog */
        $this->AddAgent(
            'Dna\Kotmegapolis\Agent\Catalog\Edding::export();',
            'N',
            60 * 60 * 4,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString()
        );
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Catalog\Edding::donwload();', 60 * 60 * 4);
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Catalog\Edding::import();', 15 * 60);

        /* Price */
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Price\Import::update();', 30);
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Price\Export::markerland();', 5 * 60);
        $this->InstallAgentTime('Dna\Kotmegapolis\Agent\Price\Export::rotringshop();', 5 * 60);
        $this->AddAgent(
            'Dna\Kotmegapolis\Agent\Price\Export::clear();',
            'Y',
            60 * 60 * 24,
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->toString(),
            'Y',
            (new DateTime(date("Y-m-d 03:00:00"), "Y-m-d H:i:s"))->add('1 day')->toString()
        );
    }

    public function InstallAgentTime($name, $interval)
    {
        $this->AddAgent(
            $name,
            'N',
            $interval,
            (new DateTime())->toString(),
            'Y',
            (new DateTime())->toString()
        );
    }

    public function UnInstallAgent()
    {
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Oper::markerland();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Oper::rotringshop();', $this->MODULE_ID);

        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Catalog\Edding::export();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Catalog\Edding::donwload();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Catalog\Edding::import();', $this->MODULE_ID);

        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Price\Import::update();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Price\Export::markerland();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Price\Export::rotringshop();', $this->MODULE_ID);
        CAgent::RemoveAgent('Dna\Kotmegapolis\Agent\Price\Export::clear();', $this->MODULE_ID);
        return true;
    }

}
